import React from 'react'
import Navbar from '../components/Navbar'
import '../index.css';
import Footer from '../components/Footer'

export default class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            length: 0,
            products: [],
        };
    }

    componentDidMount() {
        fetch('https://fakestoreapi.com/products?limit=9')
            .then((res) => res.json())
            .then((res) =>
                this.setState({
                    length: res.length,
                    products: res,
                }),
            );
    }

    render() {
        const { products } = this.state;
        const productsList = products.map((product) => (
            <div key={product.id} className="card col-sm-3 m-5 bg-secondary">
                <img
                    src={product.image}
                    className="card-img-top mt-3"
                    alt={product.id}
                    width="100%"
                    height="300"
                />
                <div className="card-body">
                    <h5 className="card-title">{product.title}</h5>
                    <h6 className="card-text"><b>$ {product.price}</b></h6>
                    <button className="btn btn-primary"><b>Buy</b></button>
                </div>
            </div>
        ));

        return (
            <React.Fragment>
                <Navbar />
                <div className="container-fluid mt-5 text-white text-center">
                <h2><u>Products Of The Day</u></h2>
                    <div className="row justify-content-around">
                        {productsList}
                    </div>
                </div>
                <Footer />
            </React.Fragment>
        );
    }
}
