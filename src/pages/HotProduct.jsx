import React from 'react'
import Navbar from '../components/Navbar'
import './Hotproduct.css';
import ReviewItems from '../components/Reviews';
import Footer from '../components/Footer'

export default function HotProduct() {
    return (
    <div>
        <Navbar />
        <div className="container-fluid mt-5">
    <h1>Hot Product Today</h1>
    <div className="Parentbox">
      <FotoProduk />
      <ProdukInfo isDiscount="yes" name="Lock and Love Women's Removable Hooded Faux Leather Moto Biker Jacket" category="CLOTHES" />
      <ReviewItems />
    </div>
    </div>
    <Footer />
    </div>
  );
}

function FotoProduk() {
  return (
    <div className="Foto">
      <img src="https://fakestoreapi.com/img/81XH0e8fefL._AC_UY879_.jpg" alt="gambar"/>
    </div>
  );
}

function CheckDiscount(props) {
  const { isDiscount } = props;
  if (isDiscount === "yes") {
    return (
      <p>Discount 50% Off</p>
    );
  }
  else if (isDiscount === "coming") {
    return (
      <p>There's will discount ...</p>
    );
  }
  else {
    return (
      <p>Not dicount Yet</p>
    );
  }
}

function ProdukInfo(props) {
  const { category, name, isDiscount } = props;
  const benefits = ["Comfortable Wear", "Stylish Desing", "Warm"];
  const listBenefits = benefits.map((itemBenefit) =>
    <li>{itemBenefit}</li>
  );
  return (
    <div>
      <div className="Deskripsi">
        <p className="Cate">{category}</p>
        <h2 className="Title">{name}</h2>
        <p className="Price"><b>$ 29.95</b></p>
        <CheckDiscount isDiscount={isDiscount} />
        <p className="Info">
        100% POLYURETHANE(shell) 100% POLYESTER(lining) 75% POLYESTER 25% COTTON (SWEATER), Faux leather material for style and comfort / 2 pockets of front, 2-For-One Hooded denim style faux leather jacket, Button detail on waist / Detail stitching at sides, HAND WASH ONLY / DO NOT BLEACH / LINE DRY / DO NOT IRON
        </p>
        <ul>
          {listBenefits}
        </ul>
        <a onClick={(e) => TambahCart(name, e)} href="Basket">Add to Cart</a>
      </div>
    </div>
  );
}

function TambahCart(e) {
  return console.log("Membeli " + e);
}