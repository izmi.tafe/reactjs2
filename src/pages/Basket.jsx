import React, { useState, useEffect } from 'react'
import Navbar from '../components/Navbar'
import './Basket.css'
import Footer from '../components/Footer'

export default function Basket() {
    const [hitung, setHitung] = useState(1);
    
    const kalkulasi = () => {
    setHitung(hitung + 1);
    }
    
    const kalkulasi1 = () => {
    setHitung(hitung - 1);
    }

    useEffect((e) => {
        document.title = `Your Basket Now = ${hitung}`;
    });

    const Products = 
        {
                "id":1,
                "title":"Silicon Power 256GB SSD 3D NAND A55 SLC Cache Performance Boost SATA III 2.5",
                "category":"Electronics",
                "image":"https://fakestoreapi.com/img/71kWymZ+c+L._AC_SX679_.jpg"
        };

  return(
      <>
        <div>
                <Navbar />
                <div className="container mt-5">
                <h1>Your Basket</h1>
                <div className="Basket-box">
                <div className="Item">
                        <img src={Products.image} alt="gambar"/>
                            <div className="Product">
                                <h4>{Products.title}</h4>
                                <p>{Products.category}</p>
                                <br/>
                                <br/>
                                <div className="keranjang">
                                    <button onClick={kalkulasi1}>-</button><span className="jml" placeholder="1">{hitung}</span><button onClick={kalkulasi}>+</button>
                                </div>
                    </div>
                </div>
                </div>
                </div>
        </div>
        
        <Footer />
        </>
  );
}