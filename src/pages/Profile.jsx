import React, { Component } from 'react'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'

export default class Profile extends Component {
    render() {
        return (
            <>
            <div>
                <Navbar />
        <div className="container mt-5">
                <h1>Profile</h1>

                <div className="jumbotron-fluid text-center mt-5">
        <div className="container">
            <div className="row">
            <div class="col-md-12">
                    <img src="https://images.pexels.com/photos/3656518/pexels-photo-3656518.jpeg?cs=srgb&dl=pexels-luis-araujo-3656518.jpg&fm=jpg" class="rounded-circle fp" alt="Foto Profil" width="250" height="250"/>
                </div>
                <div className="col-md-12 mt-5">
                <h2>Calvin Seo</h2>
                <br />
                        <h4><small>Address: 500 Terry Francois Street, San Francisco, CA 94158</small></h4>
                        <h5><small>E-mail: info@mysite.com</small></h5>
                        <h5><small>Phone: 123-456-7890</small></h5>
                </div>
            </div>
        </div>
    </div>
        </div>
        </div>
        <Footer />
        </>
        )
    }
}
