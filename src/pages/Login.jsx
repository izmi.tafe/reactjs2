import React from 'react';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nama: '',
            password: '',
            action: '/Home',
        };
    }

    handleChange(event) {
        const inputValueNama = event.target.value;
        const inputValuePass = event.target.value;
        this.setState({
            nama: inputValueNama,
            password: inputValuePass,
        });
    }

    handleSubmit() {
        if (this.state.password !== 'belovely') {
            alert('Password Anda Salah!');
            this.setState({ action: '/' });
        }
    }

    render() {
        return (
            <div className="login container-fluid">
                <div className="row">
                <div className="col-md-12 text-center">
                <h1>DigiStore</h1>
                <h3>Please fill your username and password below</h3>
                </div>
                    <div className="col-sm-8 mx-auto mt-5">
                        <form
                            action={this.state.action}
                            method="get"
                            onSubmit={() => {
                                this.handleSubmit();
                            }}
                        >
                            <div className="form-group">
                                <label for="uname">
                                    <b>Username</b>
                                </label>
                                <input
                                    onChange={(event) => {
                                        this.handleChange(event);
                                    }}
                                    className="form-control"
                                    type="email"
                                    placeholder="Enter your email"
                                    name="uname"
                                    id="uname"
                                    required
                                    value={this.state.value}
                                />
                            </div>
                            <br />
                            <div className="form-group">
                                <label for="psw">
                                    <b>Password</b>
                                </label>
                                <input
                                    onChange={(event) => {
                                        this.handleChange(event);
                                    }}
                                    className="form-control"
                                    type="password"
                                    placeholder="Enter Password"
                                    name="psw"
                                    id="psw"
                                    required
                                    value={this.state.value}
                                />
                            </div>
                            <br />

                            <button className="mb-4" type="submit">
                                Login
                            </button>
                            <br />

                            <span className="psw ">
                            <a href="https://google.com">Forgot password?</a>
                            </span>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
