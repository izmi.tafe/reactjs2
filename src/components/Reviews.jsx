import React from 'react';

function Reviews() {
    // mempersiapkan data dummy JSON
    const users = [
        {
            "id": 1,
            "name": "Christopher Bang",
            "review": "My bestfriend was so happy to wearing this, very comfortable she said",
            "photo": "https://images.pexels.com/photos/2232981/pexels-photo-2232981.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
        },
        {
            "id": 2,
            "name": "Shannon Williams",
            "review": "Being my favorite jackets!!!",
            "photo": "https://images.pexels.com/photos/764529/pexels-photo-764529.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
        },
        {
            "id": 3,
            "name": "Maureen Rae",
            "review": "Quality at the point!!",
            "photo": "https://images.pexels.com/photos/2100063/pexels-photo-2100063.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
        }
    ];
    const listReview = users.map((itemReview) =>
        <div className="Item">
            <img src={itemReview.photo} alt="gambar"/>
            <div className="User">
                <h3>{itemReview.name}</h3>
                <p>{itemReview.review}</p>
            </div>
        </div>
    );
    return (
        <div className="Review-box">
            <h2>Reviews</h2>
            {listReview}
        </div>
    );
}

export default Reviews;