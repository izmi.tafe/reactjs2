import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <div class="container-fluid kontainer">
        <div class="row">
            <div class="col-sm-12">
                <div class="copyright-text">
                    <p class="p-2 text-light bg-dark text-center position-bottom">
                    © 2020 Digitalent. All rights reserved. Created by Izmi H.
                    </p>
                </div>
            </div>
        </div>
    </div>
        )
    }
}
