import React from 'react';

class Timer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nilaiInput: '',
            nilaiTampil: 0,
        };
    }

    handleChange(event) {
        const inputValue = event.target.value;
        this.setState({
            nilaiInput: parseInt(inputValue),
            nilaiTampil: parseInt(inputValue) * 60,
        });
    }

    timer() {
        this.interval = setInterval(() => {
            this.setState({ nilaiTampil: this.state.nilaiTampil - 1 });
        }, 1000);
    }

    stop() {
        clearInterval(this.interval);
    }

    render() {
        if (isNaN(this.state.nilaiTampil)) {
            this.state.nilaiTampil = 0;
        }

        return (
            <React.Fragment>
                <div className="container mt-5">
                <h3>Aplikasi Timer Sederhana</h3>
                <div className="form-group mx-sm-3 mb-5 mt-5">
                <label className="sr-onlyt"><h5>Masukkan menit yang akan diterapkan :</h5></label>
  
                <input className="form-control w-100 p-3"
                    onChange={(event) => {
                        this.handleChange(event);
                    }}
                    type="number"
                    placeholder="0"
                    value={this.state.nilaiInput}
                />
                </div>

                <div className="badge badge-secondary"><h5>{this.state.nilaiTampil} detik</h5></div>
                <br /><br />
                <div className="btn-group btn-group-lg" role="group" aria-label="Basic example">
                <button type="button" className="btn btn-secondary"
                    onClick={() => {
                        this.timer();
                    }}
                >
                    Start
                </button>

                <button type="button" className="btn btn-secondary"
                    onClick={() => {
                        this.stop();
                    }}
                >
                    Stop
                </button>
                </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Timer;
