import React from 'react';
import {NavLink} from 'react-router-dom';

class Navbar extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <a className="navbar-brand" href="Home">
                    DigiStore
                </a>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div
                    className="collapse navbar-collapse"
                    id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                        <NavLink to ="Home" className="nav-link" href="#">Home</NavLink>
                        </li>
                        <li className="nav-item">
                        <NavLink to ="HotProduct" className="nav-link" href="#">Hot Products</NavLink>
                        </li>
                        <li className="nav-item">
                        <NavLink to ="Basket" className="nav-link" href="#">Basket</NavLink>
                        </li>
                        <li className="nav-item">
                        <NavLink to ="Profile" className="nav-link" href="#">Profile</NavLink>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Navbar;
