import React from 'react'
import {BrowserRouter, Route} from 'react-router-dom'
// import Navbar from './components/Navbar'
import Login from './pages/Login'
import HotProduct from './pages/HotProduct'
import Basket from './pages/Basket'
import Profile from './pages/Profile'
import Home from './pages/Home'
// import App from './App'

function Router(){
    return (
        <BrowserRouter>
           {/* <Navbar /> */}
            <Route exact path="/Home" component={Home}/>
            <Route exact path="/HotProduct" component={HotProduct}/>
            <Route exact path="/Profile" component={Profile}/>
            <Route exact path="/Basket" component={Basket}/>
            <Route exact path="/" component={Login}/>
        </BrowserRouter>
    )
}

export default Router
